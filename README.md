
# SB Charts

Contains umbrella charts in order to install the SB solution on a kubernetes cluster.

## Prerequisites

- A Kubernetes cluster (`< 1.22`)
- [Helm](https://helm.sh/docs/intro/install/)

> The Kubernetes cluster must be < 1.22 due to dependencies (Multicluster scheduler).

## Installation

### Via Script

```bash
./install.sh
```

> The installation script will check the presence of `helm` and `kubectl`. 
> Then, it'll install the following components :
> - CertManager
> - Admiralty
> - OpenCloud Umbrella chart

### Manual

#### Step 1 : Cert-manager

```bash
helm repo add jetstack https://charts.jetstack.io && \
helm install \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --create-namespace \
  --version v1.4.0 \
  --set installCRDs=true
```
> _Cert manager_ will be installed in a dedicated namespace and cannot be installed during the base umbrella chart

#### Step 2 : Admiralty

```bash
helm repo add admiralty https://charts.admiralty.io && \
helm install \
  admiralty admiralty/multicluster-scheduler \
  --namespace admiralty \
  --create-namespace \
  --version 0.13.2 \
  --wait
```

> _Admiralty_ will be installed in a dedicated namespace and cannot be installed during the base umbrella chart

#### Step 3 - Umbrella Chart
Add repo
```bashpipeline-orchestrator-6c5c47f79f-4b6r8
helm repo add irtsb http://irtsb.ml/api/helm-registry
```

```bash
helm install  irtsb irtsb/sb-services  --wait
```

> Note that the installation can take a while due to the pull of all the docker images. In case of fail due to a timeout,
> try `helm upgrade --install irtsb irtsb/sb-services`.
 
#### Step 4 (Optional) :  Telemetry tools and providers

For additional providers (sentinel and minio)

> Note: it might be required to modify the values depending on your configuration of the deployment
```bash
helm install  sentinel-provider irtsb/sentinel-provider  --wait
helm install  provider-minio irtsb/provider-minio  --set minioSecretKeyRef.name=minio --set config.service.minioHost=minio:9000 --wait
```

For telemetry tools:
```bash 
helm install opentelemetry-operator opentelemetry-operator \
  --repo https://open-telemetry.github.io/opentelemetry-helm-charts \
  --version 0.1.0 \
  --wait --debug
```

```bash
helm install irtsb ./charts/telemetry-tools
```

