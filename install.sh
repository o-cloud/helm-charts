#!/usr/bin/env bash

# Check prerequisites
if ! command -v helm &> /dev/null
then
    echo "helm could not be found. See https://helm.sh/docs/intro/install/"
    exit 1
fi

if ! command -v kubectl &> /dev/null
then
    echo "kubectl could not be found. Install kubectl and connect it to a running cluster"
    exit 1
fi


echo "***"
echo "*** Step 1: Install cert-manager"
helm install \
  cert-manager cert-manager \
  --namespace cert-manager \
  --create-namespace \
  --version v1.4.0 \
  --repo https://charts.jetstack.io \
  --set installCRDs=true

echo "***"
echo "*** Step 2: Install Admiralty"
helm install \
  admiralty multicluster-scheduler \
  --namespace admiralty \
  --create-namespace \
  --version 0.13.2 \
  --repo https://charts.admiralty.io \
  --wait

echo "***"
echo "*** Step 3: Install Open Cloud"
helm install irtsb ./charts/sb-services

echo "*** Installation complete ***"
helm ls -A
